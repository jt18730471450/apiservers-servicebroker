define BOILERPLATE
/*
@asiainfo.com
*/

endef
export BOILERPLATE

define GITIGNORE
bin
pkg/client
pkg/**/*generated*
docs/build
docs/static_includes
config
apiserver.local.config
default.etcd
kubeconfig

endef
export GITIGNORE

##############################

.PHONY: default
default: build

.PHONY: gen
gen:
	apiserver-boot build generated

.PHONY: clean
clean:
	rm -rf bin pkg/client
	find pkg -type f -name "*generated*" -delete

.PHONY: test
test:
	go test ./pkg/... ./cmd/...

.PHONY: docs
docs:
	apiserver-boot build docs

.PHONY: build
build:
	apiserver-boot build executables

.PHONY: image
image:
	apiserver-boot build container \
		--image apiservers-servicebroker \
		--generate false

.PHONY: local-config
local-config:
	apiserver-boot build config \
		--name apiservers-servicebroker \
		--namespace default \
		--local-minikube

.PHONY: cluster-config
cluster-config:
	apiserver-boot build config \
		--name apiservers-servicebroker \
		--namespace default \
		--image apiservers-servicebroker:lastest

.PHONY: run-local
run-local:
	apiserver-boot run local --generate=false
	# --apiserver=
	# --controller-manager=

.PHONY: run-cluster
run-cluster:
	apiserver-boot run in-cluster --generate=false \
		--name nameofservicetorun --namespace default \
		--image apiservers-servicebroker:lastest

.PHONY: boot
boot: # to create the skeleton of this project.
	echo "$$GITIGNORE" > .gitignore
	echo "$$BOILERPLATE" > boilerplate.go.txt
	apiserver-boot init repo --domain asiainfo.com
	apiserver-boot create group version resource --non-namespaced=true \
		--group prd --version v1 --kind ServiceBroker
	apiserver-boot create group version resource --non-namespaced=true \
		--group prd --version v1 --kind BackingService
	apiserver-boot create group version resource --non-namespaced=false \
		--group prd --version v1 --kind BackingServiceInstance
	apiserver-boot create subresource --subresource binding \
		--group prd --version v1 --kind BackingServiceInstance
	