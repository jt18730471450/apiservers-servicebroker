/*
@asiainfo.com
*/

package backingservice

import (
	"log"

	"github.com/kubernetes-incubator/apiserver-builder/pkg/builders"
	"k8s.io/client-go/rest"

	"github.com/asiainfoldp/apiservers-servicebroker/pkg/apis/prd/v1"
	listers "github.com/asiainfoldp/apiservers-servicebroker/pkg/client/listers_generated/prd/v1"
	"github.com/asiainfoldp/apiservers-servicebroker/pkg/controller/sharedinformers"
)

// +controller:group=prd,version=v1,kind=BackingService,resource=backingservices
type BackingServiceControllerImpl struct {
	builders.DefaultControllerFns

	// lister indexes properties about BackingService
	lister listers.BackingServiceLister
}

// Init initializes the controller and is called by the generated code
// Registers eventhandlers to enqueue events
// config - client configuration for talking to the apiserver
// si - informer factory shared across all controllers for listening to events and indexing resource properties
// queue - message queue for handling new events.  unique to this controller.
func (c *BackingServiceControllerImpl) Init(
	config *rest.Config,
	si *sharedinformers.SharedInformers,
	reconcileKey func(key string) error) {

	// Set the informer and lister for subscribing to events and indexing backingservices labels
	c.lister = si.Factory.Prd().V1().BackingServices().Lister()
}

func (c *BackingServiceControllerImpl) Get(namespace, name string) (*v1.BackingService, error) {
	return c.lister.Get(name)
}

// Reconcile handles enqueued messages
func (c *BackingServiceControllerImpl) Reconcile(u *v1.BackingService) error {
	// Implement controller logic here
	log.Printf("Running reconcile BackingService for %s\n", u.Name)
	// todo: ...
	return nil
}
